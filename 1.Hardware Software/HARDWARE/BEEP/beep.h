#ifndef __BEEP_H
#define __BEEP_H
#include "sys.h"

#define BEEP PCout(13)

void BEEP_Init(void);

#endif
