#ifndef __DHT11_H
#define __DHT11_H	 
#include "sys.h"
 

#define DHT11_IO_IN()  {GPIOA->CRH&=0XFFFFFFF0;GPIOA->CRH|=8;}
#define DHT11_IO_OUT() {GPIOA->CRH&=0XFFFFFFF0;GPIOA->CRH|=3;}
										   
#define	DHT11_DQ_OUT PAout(8) 
#define	DHT11_DQ_IN  PAin(8)  


u8 DHT11_Init(void);
u8 DHT11_Read_Data(u8 *humiH,u8 *humiL,u8 *tempH,u8 *tempL);
u8 DHT11_Read_Byte(void);
u8 DHT11_Read_Bit(void);
u8 DHT11_Check(void);
void DHT11_Rst(void);
		 				    
#endif
