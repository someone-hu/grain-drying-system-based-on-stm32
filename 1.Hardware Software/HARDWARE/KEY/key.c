#include "key.h"
#include "delay.h"

 	    
//按键初始化函数 
//PA15和PC5 设置成输入
void KEY_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_5;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
} 
//注意此函数有响应优先级,KEY_ADD>KEY_DROP>KEY_SET>KEY_BEEP!!
u8 KEY_Scan(u8 mode)
{	 
	static u8 key_up=1;//按键按松开标志
	
	if(mode)key_up=1;  //支持连按
			  
	if(key_up&&(KEY_ADD==0 || KEY_DROP==0 || KEY_SET==0 || KEY_BEEP==0))
	{
		delay_ms(10);//去抖动 
		key_up=0;
		if(KEY_ADD==0){
			return KEY_ADD_PRES;
		}
		else if(KEY_DROP==0){
			return KEY_DROP_PRES;
		}
		else if(KEY_SET==0){
			return KEY_SET_PRES;
		}
		else if(KEY_BEEP==0){
			return KEY_BEEP_PRES;
		}		
	}
	else if(KEY_ADD==1 && KEY_DROP==1 && KEY_SET==1 && KEY_BEEP==1)
	{
		key_up=1;
	} 	     
	return 0;// 无按键按下
}

