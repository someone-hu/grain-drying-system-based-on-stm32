#ifndef __KEY_H
#define __KEY_H	 
#include "sys.h"
 	 

#define KEY_DROP PBin(5)
#define KEY_ADD PBin(6)
#define KEY_SET PBin(7)
#define KEY_BEEP PBin(8)

//#define KEY0  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_1)//读取按键0
//#define KEY1  GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_13)//读取按键1
//#define WK_UP   GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)//读取按键2 
 

#define KEY_ADD_PRES	1  
#define KEY_DROP_PRES 2
#define KEY_SET_PRES 3
#define KEY_BEEP_PRES 4

void KEY_Init(void);
u8 KEY_Scan(u8 mode); 					    
#endif
