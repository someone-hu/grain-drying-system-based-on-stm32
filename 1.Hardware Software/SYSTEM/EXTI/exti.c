#include "exti.h"
#include "key.h"
#include "delay.h"
#include "usart.h"
#include "RELAY.h"
extern u8 oled_set;
extern u8 alarmFlag;
extern u8 alarm_is_free;
extern u8 relay_free;
extern u8 relay;
void EXTIX_Init(void)
{
 
 	  EXTI_InitTypeDef EXTI_InitStructure;
 	  NVIC_InitTypeDef NVIC_InitStructure;

  	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);

	  KEY_Init();
		
		GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource7);
  	EXTI_InitStructure.EXTI_Line=EXTI_Line7;
  	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  	EXTI_Init(&EXTI_InitStructure);

		GPIO_EXTILineConfig(GPIO_PortSourceGPIOB,GPIO_PinSource8);
  	EXTI_InitStructure.EXTI_Line=EXTI_Line8;
  	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  	EXTI_Init(&EXTI_InitStructure);		 
 
  	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;	
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;		
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;		
  	NVIC_Init(&NVIC_InitStructure); 
}

 
void EXTI9_5_IRQHandler(void)
{
  delay_ms(10);
	if(KEY_SET == 0)
	{
		oled_set = 1;
	}
	if(KEY_BEEP == 0)
	{
		if(alarmFlag == 1)
		{
			relay_free = 0;
			alarm_is_free=0;
		}
		alarmFlag = 0;
		relay = 0;
	}
  EXTI_ClearITPendingBit(EXTI_Line7);
 	EXTI_ClearITPendingBit(EXTI_Line8);
}



