#include "timer.h"
#include "beep.h"
#include "RELAY.h"
#include "stdio.h"
    
char oledBuf[20];
extern u8 humidityH;
extern u8 humidityL;	
extern u8 temperatureH;
extern u8 temperatureL; 
extern u16 co;	
extern u8 beep;
extern u8 oled_set;
extern u8 alarmFlag;
extern u8 alarmFlag2;
extern u8 alarmFlag3;
u8 i;

void TIM2_Int_Init(u16 arr,u16 psc)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	TIM_TimeBaseStructure.TIM_Period = arr;
	TIM_TimeBaseStructure.TIM_Prescaler =psc; 
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); 
 
	TIM_ITConfig( 
		TIM2,
		TIM_IT_Update ,
		ENABLE
		);
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	TIM_Cmd(TIM2, ENABLE);
							 
}

void TIM2_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
		{
			TIM_ClearITPendingBit(TIM2, TIM_IT_Update  );
			if(oled_set == 0)
			{
				if(i>=0 && i<16)
				{		
					sprintf(oledBuf,"Temp:%d.%d C",temperatureH,temperatureL);			
					OLED_ShowString(0,0,(u8*)oledBuf,16);
								
					sprintf(oledBuf,"Hum :%d.%d %%",humidityH,humidityL);				
					OLED_ShowString(0,16,(u8*)oledBuf,16);							
					
					sprintf(oledBuf,"CO :%d ppm",co);				
					OLED_ShowString(0,32,(u8*)oledBuf,16);
					
					if(RELAY == 1)
					{
						sprintf(oledBuf,"RELAY : OPEN");			
						OLED_ShowString(0,48,(u8*)oledBuf,16);
					}else
					{
						sprintf(oledBuf,"RELAY : CLOSE");			
						OLED_ShowString(0,48,(u8*)oledBuf,16);
					}
					
					OLED_Refresh();
				}
				
				if(i>=16 && i<32)
				{				
					sprintf(oledBuf,"Hum :%d.%d %%",humidityH,humidityL);			
					OLED_ShowString(0,0,(u8*)oledBuf,16);
								
					sprintf(oledBuf,"CO :%d ppm",co);			
					OLED_ShowString(0,16,(u8*)oledBuf,16);
					
					if(RELAY == 1)
					{
						sprintf(oledBuf,"RELAY : OPEN");			
						OLED_ShowString(0,32,(u8*)oledBuf,16);
					}else
					{
						sprintf(oledBuf,"RELAY : CLOSE");			
						OLED_ShowString(0,32,(u8*)oledBuf,16);
					}
					
					if(beep == 1)
					{
						sprintf(oledBuf,"BEEP :OPEN");			
						OLED_ShowString(0,48,(u8*)oledBuf,16);
					}else
					{
						sprintf(oledBuf,"BEEP :CLOSE");			
						OLED_ShowString(0,48,(u8*)oledBuf,16);
					}
					
					OLED_Refresh();
				}
				
				if(i>=32 && i<48)
				{						
					sprintf(oledBuf,"CO :%d ppm",co);			
					OLED_ShowString(0,0,(u8*)oledBuf,16);
					
					if(RELAY == 1)
					{
						sprintf(oledBuf,"RELAY : OPEN");			
						OLED_ShowString(0,16,(u8*)oledBuf,16);
					}else
					{
						sprintf(oledBuf,"RELAY : CLOSE");			
						OLED_ShowString(0,16,(u8*)oledBuf,16);
					}
					
					if(beep == 1)
					{
						sprintf(oledBuf,"BEEP :OPEN");			
						OLED_ShowString(0,32,(u8*)oledBuf,16);
					}else
					{
						sprintf(oledBuf,"BEEP :CLOSE");			
						OLED_ShowString(0,32,(u8*)oledBuf,16);
					}

					sprintf(oledBuf,"Temp:%d.%d C",temperatureH,temperatureL);			
					OLED_ShowString(0,48,(u8*)oledBuf,16);
					
					OLED_Refresh();
				}
				
				if(i>=48 && i<=64)
				{	
					if(beep == 1)
					{
						sprintf(oledBuf,"BEEP :OPEN");			
						OLED_ShowString(0,0,(u8*)oledBuf,16);
					}else
					{
						sprintf(oledBuf,"BEEP :CLOSE");			
						OLED_ShowString(0,0,(u8*)oledBuf,16);
					}

					sprintf(oledBuf,"Temp:%d.%d C",temperatureH,temperatureL);			
					OLED_ShowString(0,16,(u8*)oledBuf,16);
					
					sprintf(oledBuf,"Hum :%d.%d %%",humidityH,humidityL);			
					OLED_ShowString(0,32,(u8*)oledBuf,16);
					
					sprintf(oledBuf,"CO :%d ppm",co);			
					OLED_ShowString(0,48,(u8*)oledBuf,16);
					
					OLED_Refresh();
				}	

				i=i+2;
				
				if(i%16==0)
				{
					OLED_Clear();
				}
				
				if(i == 64)
				{
					i=0;
				}
		}
	}
}

void TIM3_Int_Init(u16 arr,u16 psc)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	TIM_TimeBaseStructure.TIM_Period = arr; 
	TIM_TimeBaseStructure.TIM_Prescaler =psc;   
	TIM_TimeBaseStructure.TIM_ClockDivision = 0; 
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
 
	TIM_ITConfig(  
		TIM3, 
		TIM_IT_Update ,
		ENABLE 
		);
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn; 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; 
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3; 
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
	NVIC_Init(&NVIC_InitStructure);

	TIM_Cmd(TIM3, ENABLE);
							 
}

void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) 
		{
			TIM_ClearITPendingBit(TIM3, TIM_IT_Update  );
			if(alarmFlag == 1 || alarmFlag2 == 1 || alarmFlag3 == 1)
			{
				BEEP = !BEEP;
				beep = 1;
			}
			else
			{
				BEEP=1;
				beep = 0;
			}
		}
}












