#include "delay.h"
#include "sys.h"
#include "dht11.h"
#include "beep.h"
#include "timer.h"
#include "oled.h"
#include "esp8266.h"
#include "usart.h"
#include "onenet.h"
#include <string.h>
#include <stdlib.h>
#include "adc.h"
#include "key.h"
#include "exti.h"
#include "RELAY.h"

u8 alarmFlag;
u8 alarmFlag2 = 0;
u8 alarmFlag3 = 0;
u8 alarm_is_free = 10;
u8 relay_free = 10;
u8 humidityH;
u8 humidityL;	
u8 temperatureH;
u8 temperatureL;
u16 co;
u8 menu = 1;
u8 beep;
u8 oled_set = 0;
u8 key;
u8 humidityH_MAX = 80;
u8 temperatureH_MAX = 50;
u16 co_MAX = 1500;
u8 button;
u8 relay;
u8 relay2;
u8 relay3;

extern char oledBuf[20];
char PUB_BUF[256];
const char *devSubTopic[] = {"grain_2"};
const char devPubTopic[] = "grain_1";
u8 ESP8266_INIT_OK = 0;

 int main(void)
 {	
		unsigned short timeCount = 0;
		unsigned char *dataPtr = NULL;
		static u8 lineNow;
	 
		Usart1_Init(115200);
			DEBUG_LOG("\r\n");
			DEBUG_LOG("UART1初始化			[OK]");

		delay_init();
			DEBUG_LOG("\r\n");
			DEBUG_LOG("Delay初始化			[OK]");
	 
		OLED_Init();
		OLED_ColorTurn(0);
		OLED_DisplayTurn(0);
		OLED_Clear();
			DEBUG_LOG("OLED1初始化			[OK]");
			OLED_Refresh_Line("OLED");
	 	
		RELAY_Init();
	 		RELAY = 0;
			DEBUG_LOG("RELAY初始化			[OK]");
			OLED_Refresh_Line("RELAY");
	 
	 	BEEP_Init();
			DEBUG_LOG("蜂鸣器初始化			[OK]");
			OLED_Refresh_Line("Beep");
	 
		Usart2_Init(115200);
			DEBUG_LOG("UART2初始化			[OK]");
			OLED_Refresh_Line("Usart2");

		NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
			DEBUG_LOG("中断优先初始化			[OK]");
			OLED_Refresh_Line("NVIC");	
	
		EXTIX_Init();
			DEBUG_LOG("EXTI初始化			[OK]");
			OLED_Refresh_Line("EXTI");
		
		DHT11_Init();
			DEBUG_LOG("DHT11初始化			[OK]");
			OLED_Refresh_Line("DHT11");
		
		Adc_Init();
			DEBUG_LOG("ADC初始化			[OK]");
			OLED_Refresh_Line("ADC");
		
		KEY_Init();
			DEBUG_LOG("KEY初始化			[OK]");
			OLED_Refresh_Line("KEY");
		
		DEBUG_LOG("硬件初始化			[OK]");
		
		DEBUG_LOG("初始化ESP8266 WIFI模块...");
	
		if(!ESP8266_INIT_OK)
		{
			OLED_Clear();
			sprintf(oledBuf,"Waiting For");
			OLED_ShowString(16,0,(u8*)oledBuf,16);
			sprintf(oledBuf,"WiFi");
			OLED_ShowString(48,18,(u8*)oledBuf,16);
			sprintf(oledBuf,"Connection");
			OLED_ShowString(24,36,(u8*)oledBuf,16);
			OLED_Refresh();
		}
	
		ESP8266_Init();
		OLED_Clear();
	
		sprintf(oledBuf,"Waiting For");
		OLED_ShowString(16,0,(u8*)oledBuf,16);
		sprintf(oledBuf,"MQTT Server");
		OLED_ShowString(16,18,(u8*)oledBuf,16);
		sprintf(oledBuf,"Connection");
		OLED_ShowString(24,36,(u8*)oledBuf,16);
		OLED_Refresh();	
	
		while(OneNet_DevLink()){
			delay_ms(500);
		}
	
		OLED_Clear();
	
		TIM2_Int_Init(4999,7199);
		TIM3_Int_Init(2499,7199);
	
		OneNet_Subscribe(devSubTopic, 1);
	 

		while(1)
		{	
			
			key = KEY_Scan(0);
			if(key == KEY_DROP_PRES)
			{
				alarmFlag2 = !alarmFlag2;
			}
			if(key == KEY_ADD_PRES)
			{
				relay2 = !relay2;
			}
			
			if(relay2 == 1 || relay == 1 || relay3 == 1)
			{
				RELAY = 1;
			}
			else if(relay2 == 0 && relay == 0 && relay3 == 0) 
			{
				RELAY = 0;
			}
			
			if(oled_set == 1)
			{
				alarm_is_free = 10;
				alarmFlag = 0;
				alarmFlag2 = 0;
				RELAY = 0;
				OLED_Clear();
				while(1)
				{
					key = KEY_Scan(0);
					if(menu == 1)
						{
							sprintf(oledBuf,"Temp_MAX");			
							OLED_ShowString(0,0,(u8*)oledBuf,16);
							sprintf(oledBuf,"%d   ",temperatureH_MAX);			
							OLED_ShowString(0,16,(u8*)oledBuf,16);
							sprintf(oledBuf,"meun-%d", 1);			
							OLED_ShowString(0,48,(u8*)oledBuf,16);
							if(key == KEY_ADD_PRES)
							{
								temperatureH_MAX = temperatureH_MAX + 1;
								if(temperatureH_MAX >= 255)
								{
									temperatureH_MAX = 255;
								}								
							}
							else if(key == KEY_DROP_PRES)
							{
								temperatureH_MAX = temperatureH_MAX - 1;
								if(temperatureH_MAX <= 0)
								{
									temperatureH_MAX = 0;
								}
							}
							else if(KEY_SET==0)
							{	
								delay_ms(50);								
								if(KEY_SET==0)
								{
										OLED_Clear();
										menu = 2;
										break;
								}
							}
							else if(key == KEY_BEEP_PRES)
							{
								oled_set = 0;
								menu = 1;
								break;
							}
							OLED_Refresh();
						}
						
					if(menu == 2)
					{
							sprintf(oledBuf,"Hum__MAX");			
							OLED_ShowString(0,0,(u8*)oledBuf,16);
							sprintf(oledBuf,"%d   ", humidityH_MAX);			
							OLED_ShowString(0,16,(u8*)oledBuf,16);
							sprintf(oledBuf,"meun-%d", 2);			
							OLED_ShowString(0,48,(u8*)oledBuf,16);
							if(key == KEY_ADD_PRES)
							{
								humidityH_MAX = humidityH_MAX + 1;
								if(humidityH_MAX >= 255)
								{
									humidityH_MAX = 255;
								}								
							}
							else if(key == KEY_DROP_PRES)
							{
								humidityH_MAX = humidityH_MAX - 1;
								if(humidityH_MAX <= 0)
								{
									humidityH_MAX = 0;
								}
							}
							else if(KEY_SET==0)
							{
								delay_ms(50);
								if(KEY_SET==0)
								{
										OLED_Clear();
										menu = 3;
										break;
								}
							}
							else if(key == KEY_BEEP_PRES)
							{
								oled_set = 0;
								menu = 1;
								break;
							}
							OLED_Refresh();
						}
						
					if(menu == 3)
					{
							sprintf(oledBuf,"CO___MAX");			
							OLED_ShowString(0,0,(u8*)oledBuf,16);
							sprintf(oledBuf,"%d   ", co_MAX);			
							OLED_ShowString(0,16,(u8*)oledBuf,16);
							sprintf(oledBuf,"meun-%d", 3);			
							OLED_ShowString(0,48,(u8*)oledBuf,16);
							if(key == KEY_ADD_PRES)
							{
								co_MAX = co_MAX + 1;
								if(co_MAX >= 5000)
								{
									co_MAX = 5000;
								}								
							}
							else if(key == KEY_DROP_PRES)
							{
								co_MAX = co_MAX - 1;
								if(co_MAX <= 0)
								{
									co_MAX = 0;
								}
							}
							else if(KEY_SET==0)
							{
								if(KEY_SET==0)
								{
										OLED_Clear();
										menu = 1;
										break;
								}
							}
							else if(key == KEY_BEEP_PRES)
							{
								oled_set = 0;
								menu = 1;
								break;
							}
							OLED_Refresh();
						}														
				}
			}
			
			if(alarm_is_free == 10)
			{
				if((humidityH < humidityH_MAX) && (temperatureH < temperatureH_MAX) && (co < co_MAX))
				{
					alarmFlag = 0;
					relay = 0;
				}
				else 
				{
					alarmFlag = 1;
					if(temperatureH >= temperatureH_MAX)
					{
						relay = 0;
					}
					else
					{
						relay = 1;
					}
				}
			}
			if(timeCount%40==0)                  
			{
				if(alarm_is_free < 10)
				{
					alarm_is_free++;
				}
				if(relay_free < 10)
				{
					relay_free++;
				}
				co=Get_Adc(1);
				DHT11_Read_Data(&humidityH,&humidityL,&temperatureH,&temperatureL);
				UsartPrintf(USART1,"Temp:%d.%d  Hum:%d.%d  CO:%d  Beep:%d  Relay:%d",temperatureH,temperatureL,humidityH,humidityL,co,beep,RELAY);
			}
			if(++timeCount >= 200)
			{
				UsartPrintf(USART_DEBUG, "OneNet_Publish\r\n");
				sprintf(PUB_BUF,"{\"Temp\":%d.%d,\"Hum\":%d.%d,\"CO\":%d,\"Beep\":%d,\"Relay\":%d}",temperatureH,temperatureL,humidityH,humidityL,co,beep,RELAY);
				OneNet_Publish(devPubTopic, PUB_BUF);
				timeCount = 0;
				ESP8266_Clear();
			}
			
			dataPtr = ESP8266_GetIPD(3);
			if(dataPtr != NULL){
				OneNet_RevPro(dataPtr);
			}
			delay_ms(10);
		}
 }

