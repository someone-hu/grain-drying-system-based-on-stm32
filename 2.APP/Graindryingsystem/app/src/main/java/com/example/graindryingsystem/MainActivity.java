package com.example.graindryingsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private String host = "tcp://39.107.232.225:1883";
    private String userName = "android";
    private String passWord = "android";
    private String mqtt_id = "Grain_Drying_System";
    private String mqtt_sub_topic = "grain_1";
    private String mqtt_pub_topic = "grain_2";
    private ScheduledExecutorService scheduler;

    private Button btn_beep; //初始化按钮

    private TextView text_1;
    private TextView text_2;
    private TextView text_3;
    private TextView text_4;
    private TextView text_temp;
    private TextView text_hum;
    private TextView text_beep;
    private TextView text_co;
    private TextView text_hot;
    private ImageView image_hot;
    private ImageView image_beep;

    int beepNum = 0;
    int HotNum= 0;
    private MqttClient client;
    private MqttConnectOptions options;
    private Handler handler;

    @Override
    @SuppressLint("HandlerLeak")
    protected void onCreate(Bundle savedInstanceState) {
        //这里是程序界面 打开后最先运行的
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_1 = findViewById(R.id.text_1);
        text_2 = findViewById(R.id.text_2);
        text_3 = findViewById(R.id.text_3);
        text_4 = findViewById(R.id.text_4);
        text_temp = findViewById(R.id.text_temp);
        text_hum = findViewById(R.id.text_hum);
        text_beep= findViewById(R.id.text_beep);
        text_co = findViewById(R.id.text_co);
        text_hot= findViewById(R.id.text_hot);
        image_hot= findViewById(R.id.hot);
        image_beep= findViewById(R.id.beep);




        image_hot =findViewById(R.id.hot);
        image_hot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    publishmessageplus(mqtt_pub_topic,"{\"set_tool\":0}");
                     HotNum++;
                     text_4.setText("发送干燥器指令-"+HotNum);
                     if (HotNum >=10)
                     {
                         HotNum = 0;
                     }
            }
        });

        image_beep =findViewById(R.id.beep);
        image_beep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishmessageplus(mqtt_pub_topic,"{\"set_tool\":1}");

                beepNum++;
                text_3.setText("发送警报器指令-"+beepNum);
                if (beepNum >=10)
                {
                    beepNum = 0;
                }

            }
        });

        Mqtt_init();
        startReconnect();

        handler = new Handler() {
            @SuppressLint("SetTextI18n")
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what){
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        String msg_json = msg.obj.toString().substring(msg.obj.toString().indexOf("[")+1,msg.obj.toString().indexOf("]"));
                        //Toast.makeText(MainActivity.this,msg_json,Toast.LENGTH_SHORT).show();
                        try {
                            JSONObject jsonObject = new JSONObject(msg_json);
                            String Temp = jsonObject.getString("Temp");
                            String Hum = jsonObject.getString("Hum");
                            String CO = jsonObject.getString("CO");
                            int beep = jsonObject.getInt("Beep");
                            int hot = jsonObject.getInt("Relay");
                            text_temp.setText(Temp + " ℃");
                            text_hum.setText(Hum + " %");
                            text_co.setText(CO + " ppm");
                            if(beep == 1){
                                text_beep.setText("查询设备端状态-打开");
                            }
                            else if(beep == 0){
                                text_beep.setText("查询设备端状态-关闭");
                            }

                            if(hot == 1){
                                text_hot.setText("查询设备端状态-打开");
                            }
                            else if(hot == 0){
                                text_hot.setText("查询设备端状态-关闭");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        handler.removeMessages(3);
                        break;
                    case 30:  //连接失败
                        //Toast.makeText(MainActivity.this,"连接失败" ,Toast.LENGTH_SHORT).show();
                        text_1.setText("服务器连接失败！");
                        text_2.setText("Mqtt服务连接失败！");
                        break;
                    case 31:   //连接成功
                        //Toast.makeText(MainActivity.this,"连接成功" ,Toast.LENGTH_SHORT).show();
                        text_1.setText("服务器连接成功！");
                        text_2.setText("Mqtt服务连接成功！");
                        try {
                            client.subscribe(mqtt_sub_topic,1);

                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }

    private void Mqtt_init()
    {
        try {
            //host为主机名，test为clientid即连接MQTT的客户端ID，一般以客户端唯一标识符表示，MemoryPersistence设置clientid的保存形式，默认为以内存保存
            client = new MqttClient(host, mqtt_id,
                    new MemoryPersistence());
            //MQTT的连接设置
            options = new MqttConnectOptions();
            //设置是否清空session,这里如果设置为false表示服务器会保留客户端的连接记录，这里设置为true表示每次连接到服务器都以新的身份连接
            options.setCleanSession(false);
            //设置连接的用户名
            options.setUserName(userName);
            //设置连接的密码
            options.setPassword(passWord.toCharArray());
            // 设置超时时间 单位为秒
            options.setConnectionTimeout(10);
            // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
            options.setKeepAliveInterval(20);
            //设置回调
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    //连接丢失后，一般在这里面进行重连
                    System.out.println("connectionLost----------");
                    //startReconnect();
                }
                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    //publish后会执行到这里
                    System.out.println("deliveryComplete---------"
                            + token.isComplete());
                }
                @Override
                public void messageArrived(String topicName, MqttMessage message)
                        throws Exception {
                    //subscribe后得到的消息会执行到这里面
                    System.out.println("messageArrived----------");
                    Message msg = new Message();
                    msg.what = 3;   //收到消息标志位
                    msg.obj = topicName + "---" + "[" + message.toString()+"]";
                    handler.sendMessage(msg);    //hander 回传
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Mqtt_connect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(!(client.isConnected()) )  //如果还未连接
                    {
                        client.connect(options);
                        Message msg = new Message();
                        msg.what = 31;
                        handler.sendMessage(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg = new Message();
                    msg.what = 30;
                    handler.sendMessage(msg);
                }
            }
        }).start();
    }
    private void startReconnect() {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (!client.isConnected()) {
                    Mqtt_connect();
                }
            }
        }, 0 * 1000, 10 * 1000, TimeUnit.MILLISECONDS);
    }
    private void publishmessageplus(String topic,String message2)
    {
        if (client == null || !client.isConnected()) {
            return;
        }
        MqttMessage message = new MqttMessage();
        message.setPayload(message2.getBytes());
        try {
            client.publish(topic,message);
        } catch (MqttException e) {

            e.printStackTrace();
        }
    }
}