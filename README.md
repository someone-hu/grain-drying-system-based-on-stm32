# 基于STM32的粮食烘干系统

#### 介绍

- 实现粮仓内部粮堆的多点温度在线检测;
- 实现粮仓内部多点空气温湿度的在线检测;
- 数据中继节点将数据传输到管理节点和上一级网络节点;
- 控制设备节点可对排风扇进行控制以对仓内的环境进行调节;
- 系统可对各个节点和传感器进行诊断并报告;
- 人机交互节点显示监测数据，设定阈值，超过报警值后及时提醒。

#### 单片机

- STM32F103C8T6
- ESP-12S

#### 软件平台

- AndroidStudio
- keil5
- 立创EDA
- 阿里云平台
- mqtt服务

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
